<!DOCTYPE html>
<html>
<head>
    <title>Form Login | www.ngulikode.com</title>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css' ?>">
    <script src="<?php echo base_url('assets/jquery-3.3.1.min.js'); ?>" charset="utf-8"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/web-fonts-with-css/css/fontawesome-all.min.css' ?>">
    <script src="<?php echo base_url().'assets/web-fonts-with-css/svg-with-js/js/fontawesome-all.min.js' ?>" charset="utf-8"></script>

</head>
<style media="screen">

</style>
<body>
  <div class="nav-side-menu">
      <div class="brand">Brand Logo</div>
      <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
      <div class="menu-list">
          <ul id="menu-content" class="menu-content collapse out">
              <li>
                  <a href="#">
                      <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
              </li>
              <li data-toggle="collapse" data-target="#products" class="collapsed active">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> UI Elements </span></a>
              </li>
              <ul class="sub-menu collapse" id="products">
                  <li class="active"><a href="#">CSS3 Animation</a></li>
                  <li><a href="#">General</a></li>
                  <li><a href="#">Buttons</a></li>

              </ul>
              <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Services </a>
              </li>
              <ul class="sub-menu collapse" id="service">
                  <li>New Service 1</li>
                  <li>New Service 2</li>

              </ul>


              <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> New </span></a>
              </li>
              <ul class="sub-menu collapse" id="new">
                  <li>New New 1</li>

              </ul>
              <li>
                  <a href="#">
                      <i class="fa fa-user fa-lg"></i> Profile
                  </a>
              </li>
              <li>
                  <a href="#">
                      <i class="fa fa-users fa-lg"></i> Users
                  </a>
              </li>
          </ul>
      </div>
  </div>
  <!-- MAIN CONTENT -->

  <div class="container rounded shadow py-0 m-2" id="main" style="width:calc(100% - 150px)">
    <div class="navbar navbar-expand-lg navbar-dark bg-dark rounded shadow p-1 m-0" >
      <a class="navbar-brand" href="<?php echo base_url().'Admin' ?>">
        <h5><i class="fa fa-globe"></i> Aplikasi</h5>
      </a>
      <div class="mr-sm-2 navbar-nav ml-auto" style="float:right">
        <a href="<?php echo base_url(); ?>Controller/logout" style="color:white"><h5><i class="fa fa-users"></i> Logout</h5></a>
      </div>

    </div>
      <div class="row" style="margin-left: -10px">
          <div class="col-lg-12">

            <h1>Login berhasil !</h1>
            <h2>Selamat Datang, <?php echo $this->session->userdata("nama"); ?></h2>


<table class="table rounded shadow"  >
  <thead>
    <tr>
      <th>username</th>
      <th>surah</th>
      <th>ayat</th>

    </tr>
  </thead>
<tbody>
  <tr>
    <td><?php echo $this->session->userdata("nama"); ?></td>
    <td><?php echo $this->session->userdata("surat"); ?></td>
    <td><?php echo $this->session->userdata("ayat"); ?></td>
  </tr>
</tbody>





    <script src="<?php echo base_url().'assets/js/bootstrap.js' ?>" charset="utf-8"></script>

</body>
</html>
